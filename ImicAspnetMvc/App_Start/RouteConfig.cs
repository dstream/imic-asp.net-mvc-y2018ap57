﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ImicAspnetMvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ArticleDetail",
                url: "article/{id}", //Artticle/3 -> Controller: Article, Action: Index, id: 3
                defaults: new { controller = "Article", action = "Detail"},
                namespaces: new string[] { "ImicAspnetMvc.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}", //Home/about -> Controller: Home, Action: Contact
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "ImicAspnetMvc.Controllers" }
            );            
        }
    }
}
