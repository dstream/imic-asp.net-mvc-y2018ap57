﻿using ImicAspnetMvc.Areas.Manage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImicAspnetMvc.Areas.Manage.Controllers
{
    public class AccountController : BaseController
    {
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login(string q)
        {
            var queryQ = Request.QueryString["q"];

            ViewBag.queryQ = queryQ;

            ViewBag.q = q;

            if (queryQ == "text")
            {
                Response.Write("{ key: 1, key2: 2 }");
                Response.Headers.Add("ContentType", "application/json");
                Response.End();                
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel input)
        {            
            if (ModelState.IsValid)
            {
                if (input.Username.ToLower() == "admin" && input.Password == "123qwe")
                {
                    //SESSION
                    SiteHelper.CurrentAdminUser = input;
                    return RedirectToAction("Index", "Manage");
                }
                else
                {
                    input.ErrorMessage = "Invalid username or password, please try again";
                    return View(input);
                }
            }
            else
            {
                input.ErrorMessage = "Invalid action, please try again later";
                return View(input);
            }
        }

        public ActionResult Logout()
        {
            SiteHelper.CurrentAdminUser = null;
                        
            return RedirectToAction("Login");
        }        
    }
}