﻿using ImicAspnetMvc.Areas.Manage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImicAspnetMvc.Database;
using ImicAspnetMvc.Database.Entity;

namespace ImicAspnetMvc.Areas.Manage.Controllers
{
    public class ArticleCategoryController : BaseController
    {
        // GET: Manage/Article
        public ActionResult Index(int? pageIndex, string orderBy, string keyword)
        {
            RequireAdminUser();
            if (pageIndex == null || pageIndex < 1)
            {
                pageIndex = 1;
            }

            var model = new PagingModel<ArticleCategoryModel>
            {
                CurrentPage = pageIndex.Value,
                PageSize = 5,
                OrderBy = orderBy == null ? string.Empty : orderBy.ToLower(),
                Keyword = keyword,
                Records = new List<ArticleCategoryModel>()
            };

            var dbContext = new ImicDataContext();            
            var articleCategoryListFromDatabase = dbContext.ArticleCategories.AsQueryable();            

            //search by keyword
            if (!string.IsNullOrWhiteSpace(model.Keyword))
            {
                model.Keyword = model.Keyword.Trim().ToLower();

                articleCategoryListFromDatabase = articleCategoryListFromDatabase
                    .Where(e => e.Name.ToLower().Contains(model.Keyword) || e.Description.ToLower().Contains(model.Keyword)
                    || e.Id.ToString() == model.Keyword);
            }

            model.TotalRecord = articleCategoryListFromDatabase.Count();

            //order by
            switch (model.OrderBy)
            {                
                case "name":
                    articleCategoryListFromDatabase = articleCategoryListFromDatabase.OrderBy(e => e.Name);
                    break;
                case "description":
                    articleCategoryListFromDatabase = articleCategoryListFromDatabase.OrderBy(e => e.Description);
                    break;
                default:
                    articleCategoryListFromDatabase = articleCategoryListFromDatabase.OrderBy(e => e.Id);
                    break;
            }

            model.Records = articleCategoryListFromDatabase
                .Skip((model.CurrentPage - 1) * model.PageSize)
                .Take(model.PageSize).ToList().Select(e => new ArticleCategoryModel(e)).ToList();            

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            RequireAdminUser();            
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns>JSON { Status: true/false, Message: "Error message here", Data: null }</returns>
        [HttpPost]
        public ActionResult Create(ArticleCategoryModel model)
        {
            var result = new AjaxModel();
            if (ModelState.IsValid)
            {                
                //validate
                if (model.IsValid()) {
                    var dbContext = new ImicDataContext();

                    if (IsExist(dbContext, model.Name))
                    {
                        result.Status = false;
                        result.Message = $"{model.Name} was exist, please pick another name";                        
                        return Json(result);
                    }

                    var newEntity = model.ToEntity();
                    dbContext.ArticleCategories.Add(newEntity);                   

                    dbContext.SaveChanges();

                    result.Status = true;
                    result.Data = newEntity;
                }
                else
                {
                    result.Status = false;
                    result.Message = "Invalid data, please try again";                    
                }
            }
            else
            {
                result.Status = false;
                result.Message = "Unexpected error occurred, please try again later";
            }
            return Json(result);
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            RequireAdminUser();
            if (id > 0)
            {
                var dbContext = new ImicDataContext();
                var entity = dbContext.ArticleCategories.FirstOrDefault(e => e.Id == id);
                if(entity != null)
                {
                    var model = new ArticleCategoryModel(entity);                                       

                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }            
        }

        [HttpPost]
        public ActionResult Update(ArticleCategoryModel model)
        {
            RequireAdminUser();
            if (ModelState.IsValid)
            {
                //validate
                if (model.IsValid())
                {
                    var dbContext = new ImicDataContext();                   
                    var entity = dbContext.ArticleCategories.FirstOrDefault(e => e.Id == model.Id);                    
                    if (entity != null)
                    {
                        if (model.Name.ToLower() != entity.Name.ToLower() && IsExist(dbContext, model.Name))
                        {
                            ViewBag.ErrorMessage = $"{model.Name} was exist, please pick another name";
                            return View(model);
                        }

                        entity.Name = model.Name;
                        entity.Description = model.Description;                        

                        dbContext.SaveChanges();
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ErrorMessage = "Invalid data, please try again";
                }
            }
            else
            {
                //show a error message
                ViewBag.ErrorMessage = "Unexpected error occurred, please try again later";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id, string redirect)
        {
            RequireAdminUser();
            if (id > 0)
            {
                var dbContext = new ImicDataContext();
                var entity = dbContext.ArticleCategories.FirstOrDefault(e => e.Id == id);
                if (entity != null)
                {
                    dbContext.ArticleCategories.Remove(entity);
                    dbContext.SaveChanges();
                }
            }           
            return Redirect(redirect);
        }

        #region Private functions

        bool IsExist(ImicDataContext dbContext, string name)
        {            
            return dbContext.ArticleCategories.Count(e=>e.Name.ToLower() == name.ToLower()) != 0;
        }

        #endregion
    }
}