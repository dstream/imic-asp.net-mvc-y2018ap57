﻿using ImicAspnetMvc.Areas.Manage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImicAspnetMvc.Database;
using ImicAspnetMvc.Database.Entity;

namespace ImicAspnetMvc.Areas.Manage.Controllers
{
    public class ArticleController : BaseController
    {
        // GET: Manage/Article
        public ActionResult Index(int? pageIndex, string orderBy, string keyword)
        {
            RequireAdminUser();
            if (pageIndex == null || pageIndex < 1)
            {
                pageIndex = 1;
            }

            var model = new PagingModel<ArticleListModel>
            {
                CurrentPage = pageIndex.Value,
                PageSize = 5,
                OrderBy = orderBy == null ? string.Empty : orderBy.ToLower(),
                Keyword = keyword,
                Records = new List<ArticleListModel>()
            };

            var dbContext = new ImicDataContext();            
            var articleListFromDatabase = dbContext.Articles.AsQueryable();            

            //search by keyword
            if (!string.IsNullOrWhiteSpace(model.Keyword))
            {
                model.Keyword = model.Keyword.Trim().ToLower();

                articleListFromDatabase = articleListFromDatabase
                    .Where(e => e.Title.ToLower().Contains(model.Keyword) || e.Sapo.ToLower().Contains(model.Keyword)
                    || e.Id.ToString() == model.Keyword);
            }

            model.TotalRecord = articleListFromDatabase.Count();

            //order by
            switch (model.OrderBy)
            {                
                case "title":
                    articleListFromDatabase = articleListFromDatabase.OrderByDescending(e => e.Title);
                    break;
                case "sapo":
                    articleListFromDatabase = articleListFromDatabase.OrderByDescending(e => e.Sapo);
                    break;
                default:
                    articleListFromDatabase = articleListFromDatabase.OrderBy(e => e.Id);
                    break;
            }

            var dbAritlces = articleListFromDatabase
                .Skip((model.CurrentPage - 1) * model.PageSize)
                .Take(model.PageSize).ToList();
            model.Records = dbAritlces.Select(e=> new ArticleListModel(e)).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            RequireAdminUser();
            var context = new ImicDataContext();
            ViewBag.ArticleCategories = context.ArticleCategories.ToList()
                .Select(e => new ArticleCategoryModel(e));
            return View();
        }

        [HttpPost]
        public ActionResult Create(ArticleModel the_nao_cung_duoc)
        {
            RequireAdminUser();
            if (ModelState.IsValid)
            {
                //validate
                if(the_nao_cung_duoc.IsValid()) {
                    var dbContext = new ImicDataContext();
                    dbContext.Articles.Add(the_nao_cung_duoc.ToEntity());

                    //var entityToEdit = dbContext.Articles.FirstOrDefault(e => e.Id == 1);
                    //entityToEdit.Sapo = "[Updated]" + entityToEdit.Sapo;

                    dbContext.SaveChanges();

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ErrorMessage = "Invalid data, please try again";
                }
            }
            else
            {
                //show a error message
                ViewBag.ErrorMessage = "Unexpected error occurred, please try again later";
            }
            return View();
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            RequireAdminUser();
            if (id > 0)
            {
                var dbContext = new ImicDataContext();
                var entity = dbContext.Articles.FirstOrDefault(e => e.Id == id);
                if(entity != null)
                {
                    var model = new ArticleModel(entity);
                    
                    ViewBag.ArticleCategories = dbContext.ArticleCategories.ToList()
                        .Select(e=> new ArticleCategoryModel(e));

                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }            
        }

        [HttpPost]
        public ActionResult Update(ArticleModel model)
        {
            RequireAdminUser();
            if (ModelState.IsValid)
            {
                //validate
                if (model.IsValid())
                {
                    var dbContext = new ImicDataContext();
                    var entity = dbContext.Articles.FirstOrDefault(e => e.Id == model.Id);                    
                    if (entity != null)
                    {
                        entity.Title = model.Title;
                        entity.Sapo = model.Sapo;
                        entity.Content = model.Content;
                        entity.ArticleCategoryId = model.ArticleCategoryId;

                        dbContext.SaveChanges();
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ErrorMessage = "Invalid data, please try again";
                }
            }
            else
            {
                //show a error message
                ViewBag.ErrorMessage = "Unexpected error occurred, please try again later";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id, string redirect)
        {
            RequireAdminUser();
            if (id > 0)
            {
                var dbContext = new ImicDataContext();
                var entity = dbContext.Articles.FirstOrDefault(e => e.Id == id);
                if (entity != null)
                {
                    dbContext.Articles.Remove(entity);
                    dbContext.SaveChanges();
                }
            }           
            return Redirect(redirect);
        }
    }
}