﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImicAspnetMvc.Areas.Manage.Controllers
{
    public class BaseController : Controller
    {
        protected void RequireAdminUser() {
            if(SiteHelper.CurrentAdminUser == null)
            {
                Response.RedirectToRoute(new { controller = "Account", action = "Login", area = "Manage" });
                Response.End();
            }
        }
    }
}