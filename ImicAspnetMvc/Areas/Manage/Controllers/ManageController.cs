﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImicAspnetMvc.Areas.Manage.Controllers
{
    public class ManageController : BaseController
    {                
        public ActionResult Index()
        {
            RequireAdminUser();            
                        
            return View();
        }        
    }
}