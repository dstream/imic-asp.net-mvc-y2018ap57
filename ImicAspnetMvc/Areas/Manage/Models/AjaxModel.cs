﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Areas.Manage.Models
{
    public class AjaxModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public Object Data { get; set; }
    }
}