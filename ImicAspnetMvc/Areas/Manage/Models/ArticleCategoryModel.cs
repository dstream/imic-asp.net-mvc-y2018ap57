﻿using ImicAspnetMvc.Database.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Areas.Manage.Models
{
    public class ArticleCategoryModel
    {        
        public int Id { get; set; }        
        public string Name { get; set; }
        public string Description { get; set; }        

        public ArticleCategoryModel() { }

        public ArticleCategoryModel(ArticleCategory entity) {
            Id = entity.Id;
            Name = entity.Name;
            Description = entity.Description;            
        }

        public bool IsValid() {
            if (string.IsNullOrWhiteSpace(Name))
            {
                return false;
            }            
            return true;
        }

        public ArticleCategory ToEntity() {
            return new ArticleCategory
            {
                Id = Id,
                Name = Name,
                Description = Description                
            };
        }
    }
}