﻿using ImicAspnetMvc.Database.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Areas.Manage.Models
{
    public class ArticleListModel
    {        
        public int Id { get; set; }        
        public string Title { get; set; }
        public string Sapo { get; set; }
        public string Category { get; set; }

        public ArticleListModel() { }

        public ArticleListModel(Article entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Sapo = entity.Sapo;
            if (entity.ArticleCategory != null)
            {
                Category = entity.ArticleCategory.Name;
            }
        }
    }
}