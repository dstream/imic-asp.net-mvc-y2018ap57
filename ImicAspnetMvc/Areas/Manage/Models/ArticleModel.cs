﻿using ImicAspnetMvc.Database.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImicAspnetMvc.Areas.Manage.Models
{
    public class ArticleModel
    {        
        public int Id { get; set; }        
        public string Title { get; set; }
        public string Sapo { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public int ArticleCategoryId { get; set; }

        public ArticleModel() { }

        public ArticleModel(Article entity) {
            Id = entity.Id;
            Title = entity.Title;
            Sapo = entity.Sapo;
            Content = entity.Content;          //TODO: Gỡ tất cả các mã độc hại  
            ArticleCategoryId = entity.ArticleCategoryId;            
        }

        public bool IsValid() {
            if (string.IsNullOrWhiteSpace(Title))
            {
                return false;
            }
            if (string.IsNullOrWhiteSpace(Sapo))
            {
                return false;
            }
            if (string.IsNullOrWhiteSpace(Content))
            {
                return false;
            }
            if (ArticleCategoryId == 0)
            {
                return false;
            }
            return true;
        }

        public Article ToEntity() {
            return new Article
            {
                Id = Id,
                Title = Title,
                Sapo = Sapo,
                Content = Content, //TODO: Gỡ tất cả các mã độc hại
                ArticleCategoryId = ArticleCategoryId
            };
        }
    }
}