﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImicAspnetMvc.Database;
using ImicAspnetMvc.Models;

namespace ImicAspnetMvc.Controllers
{
    public class ArticleController : BaseController
    {
        public ActionResult Detail(int id)
        {
            //Request.QueryString["key1"] -> "value1"
            //Request.QueryString["key2"] -> "value2"

            if (id > 0) {                
                var dbContext = new ImicDataContext();
                var entity = dbContext.Articles.FirstOrDefault(e => e.Id == id);
                if(entity != null)
                {                    
                    SetPageTitle(entity.Title);

                    Session["ArticleId"] = id;

                    return View(new ArticleModel(entity));
                }                
            }
            return Redirect("/");
        } 
    }
}