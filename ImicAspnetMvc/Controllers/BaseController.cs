﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImicAspnetMvc.Controllers
{
    public class BaseController: Controller
    {
        protected void SetPageTitle(string pageTitle)
        {
            ViewBag.PageTitle = pageTitle;
        }
    }
}