﻿using ImicAspnetMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImicAspnetMvc.Database;
using Newtonsoft.Json;
using System.Configuration;

namespace ImicAspnetMvc.Controllers
{
    public class HomeController : BaseController
    {        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Category ID</param>
        /// <returns></returns>
        public ActionResult Index(int? id)
        {
            SetPageTitle("Home page");

            var configurationPageSize = ConfigurationManager.AppSettings["PageSize"]; // 10
            var domainName = ConfigurationManager.AppSettings["Domainname"]; // http://domain.name

            var model = new HomeModel {
                Heading = "Home",
                Articles = new List<ArticleListModel>()

            };
            var dbContext = new ImicDataContext();
            if (id > 0)
            {
                var category = dbContext.ArticleCategories.FirstOrDefault(e => e.Id == id);
                if (category != null)
                {
                    model.Heading = category.Name;
                    model.Articles = dbContext.Articles.Where(e => e.ArticleCategoryId == id).AsEnumerable().Select(e => new ArticleListModel(e)).ToList();
                }
            }
            else
            {
                model.Articles = dbContext.Articles.AsEnumerable().Select(e => new ArticleListModel(e)).ToList();
            }

            ViewBag.SessionArticleId = Session["ArticleId"];

            return View(model);
        }

        public ActionResult About(int id)
        {
            return View();
        }

        #region Partials

        public ActionResult SlidebarPartial()
        {
            var model = new SlidebarPartialModel()
            {
                Heading = "Categories",
                MostreadHeading = "Most read",
                Categories = new List<CategoryModel>(),
                MostreadArticles = new List<ArticleListModel>()
            };

            var dbContext = new ImicDataContext();
            model.Categories = dbContext.ArticleCategories.AsEnumerable().Select(e => new CategoryModel(e)).ToList();

            //most read articles

            model.MostreadArticles = dbContext.Articles.OrderByDescending(e => e.Id).Take(3).AsEnumerable().Select(e => new ArticleListModel(e)).ToList();
            
            return PartialView(model);
        }

        /// <summary>
        /// Hiện các bài đã xem gần đây, lấy từ cookies
        /// </summary>
        /// <returns></returns>
        public ActionResult ArticleRecentViewedPartial() {
            var model = new List<ArticleListModel>();

            var recentViewedCookies = Request.Cookies["RecentViewed"];// [ { id: 1, url: 'http://article.url' } ]
            if(recentViewedCookies != null && !string.IsNullOrWhiteSpace(recentViewedCookies.Value))
            {
                try
                {                    
                    var recentViewedArticles = 
                        JsonConvert.DeserializeObject<IEnumerable<ArticleRecentViewedModel>>(Server.UrlDecode(recentViewedCookies.Value));
                    var articleIds = new List<int>();
                    foreach(var item in recentViewedArticles)
                    {
                        articleIds.Add(item.id);
                    }

                    var dbContext = new ImicDataContext();

                    //model = dbContext.Articles
                    //    .Where(e => articleIds.Contains(e.Id))
                    //    .Select(e => new { e.Id, e.Sapo, e.Title }).AsEnumerable()
                    //    .Select(e => new ArticleListModel(e.Id, e.Title, e.Sapo)).ToList();

                    model = dbContext.Articles
                        .Where(e => articleIds.Contains(e.Id)).AsEnumerable()
                        .Select(e => new ArticleListModel(e)).ToList();
                }
                catch(Exception ex)
                {                    
                    Response.Cookies.Remove("RecentViewed");
                }
            }

            return PartialView(model);
        }

        #endregion
    }
}