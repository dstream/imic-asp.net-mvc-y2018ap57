﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Database.Entity
{
    public class Article
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Sapo { get; set; }

        /// <summary>
        /// virtual -> lazy loading
        /// </summary>
        public virtual string Content { get; set; }        
        public int ArticleCategoryId {get; set;}

        [ForeignKey("ArticleCategoryId")]
        public virtual ArticleCategory ArticleCategory { get; set; }
    }
}