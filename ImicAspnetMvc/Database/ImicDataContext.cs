﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ImicAspnetMvc.Database.Entity;

namespace ImicAspnetMvc.Database
{
    public class ImicDataContext: DbContext
    {
        public ImicDataContext() : base("Default")
        {
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleCategory> ArticleCategories { get; set; }
    }
}