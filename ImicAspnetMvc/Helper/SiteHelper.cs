﻿using ImicAspnetMvc.Areas.Manage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc
{
    public static class SiteHelper
    {
        public static LoginModel CurrentAdminUser
        {
            get
            {
                return (LoginModel)HttpContext.Current.Session["CurrentAdminUser"];
            }
            set
            {
                HttpContext.Current.Session["CurrentAdminUser"] = value;
            }
        }
    }
}