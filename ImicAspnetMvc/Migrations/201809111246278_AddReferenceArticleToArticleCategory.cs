namespace ImicAspnetMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReferenceArticleToArticleCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "ArticleCategoryId", c => c.Int());
            CreateIndex("dbo.Articles", "ArticleCategoryId");
            AddForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories");
            DropIndex("dbo.Articles", new[] { "ArticleCategoryId" });
            DropColumn("dbo.Articles", "ArticleCategoryId");
        }
    }
}
