namespace ImicAspnetMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDescriptionToArticleCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ArticleCategories", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ArticleCategories", "Description");
        }
    }
}
