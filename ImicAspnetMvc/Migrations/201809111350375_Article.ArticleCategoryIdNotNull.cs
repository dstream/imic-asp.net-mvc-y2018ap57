namespace ImicAspnetMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArticleArticleCategoryIdNotNull : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories");
            DropIndex("dbo.Articles", new[] { "ArticleCategoryId" });
            AlterColumn("dbo.Articles", "ArticleCategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Articles", "ArticleCategoryId");
            AddForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories");
            DropIndex("dbo.Articles", new[] { "ArticleCategoryId" });
            AlterColumn("dbo.Articles", "ArticleCategoryId", c => c.Int());
            CreateIndex("dbo.Articles", "ArticleCategoryId");
            AddForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories", "Id");
        }
    }
}
