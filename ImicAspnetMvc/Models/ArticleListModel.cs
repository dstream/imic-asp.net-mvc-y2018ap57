﻿using ImicAspnetMvc.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Models
{
    public class ArticleListModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }        
        public string Image { get; set; }
        //TODO: tags, createdBy, createdTime...

        public ArticleListModel() { }

        public ArticleListModel(Article entity) {
            Id = entity.Id;
            Title = entity.Title;
            ShortDescription = entity.Sapo;            
            Image = "http://via.placeholder.com/100x100";
        }

        public ArticleListModel(int id, string title, string sapo)
        {
            Id = id;
            Title = title;
            ShortDescription = sapo;
        }
    }
}