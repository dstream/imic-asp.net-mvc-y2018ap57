﻿using ImicAspnetMvc.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Models
{
    public class ArticleModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string Content { get; set; }
        public string Image { get; set; }
        //TODO: tags, createdBy, createdTime...

        public ArticleModel() { }

        public ArticleModel(Article entity) {
            Id = entity.Id;
            Title = entity.Title;
            ShortDescription = entity.Sapo;
            Content = entity.Content;
            Image = "http://via.placeholder.com/100x100";
        }
    }
}