﻿using ImicAspnetMvc.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Models
{
    public class ArticleRecentViewedModel
    {
        public int id { get; set; }
        public string url { get; set; }        
    }
}