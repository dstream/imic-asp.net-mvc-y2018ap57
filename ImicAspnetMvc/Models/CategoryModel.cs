﻿using ImicAspnetMvc.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public CategoryModel() { }

        public CategoryModel(ArticleCategory entity) {
            Id = entity.Id;
            Name = entity.Name;
        }
    }
}