﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Models
{
    public class HomeModel
    {
        public string Heading { get; set; }
        public List<ArticleListModel> Articles { get; set; }
    }
}