﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImicAspnetMvc.Models
{
    public class SlidebarPartialModel
    {
        public string Heading { get; set; }
        public List<CategoryModel> Categories { get; set; }
        public string MostreadHeading { get; set; }
        public List<ArticleListModel> MostreadArticles { get; set; }
    }
}