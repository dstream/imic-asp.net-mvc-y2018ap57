﻿$(function () {
    tinymce.init({
        selector: '.richtext_editor',
        theme: 'modern',
        height: 300,
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor'
        ],
        content_css: '/css/tinymce-content.css',
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
        file_browser_callback: function (field_name, url, type, win) {

            // from http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
            var w = window,
                d = document,
                e = d.documentElement,
                g = d.getElementsByTagName('body')[0],
                x = w.innerWidth || e.clientWidth || g.clientWidth,
                y = w.innerHeight || e.clientHeight || g.clientHeight;

            var cmsURL = '/vendors/RichFilemanager/index.html?exclusiveFolder=/userfiles&field_name=' + field_name;

            if (type == 'image') {
                cmsURL = cmsURL + "&filter=image";
            }

            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: 'Filemanager',
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no"
            });

        }
    });

    $(".modal-form").each(function () {
        var ths = $(this); // modal

        var form = ths.find(".modal-body form");
        form.submit(function (e) {
            e.preventDefault();

            var formData = form.serialize();
            var targetUrl = form.attr("action");
            $.post(targetUrl, formData).done(function (response) {
                console.log(response);
                if (response.Status == true) {
                    //refresh page because the listing page doesn't have ajax load
                    location.reload();
                }
                else {
                    //form[0].reset();
                    alert(response.Message);
                }
            }).error(function (response) {
                console.log(response);
            });
        });
    });
});