﻿var AddToRecentViewdList = function (articleId, articleUrl) {
    var recentViewed = Cookies.get("RecentViewed");
    //check exist in cookies
    if (typeof (recentViewed) === "undefined") {
        //non exist
        //create one
        recentViewed = [];
    }
    else {
        recentViewed = JSON.parse(recentViewed);
    }

    //check exist record
    for (var i = 0; i < recentViewed.length; i++) {
        if (recentViewed[i].id == articleId) {
            return;
        }
    }
    recentViewed.push(
        {
            id: articleId,
            url: articleUrl
        }
    );

    //set coolies
    Cookies.set("RecentViewed", recentViewed, { expired: 7 });
    console.log(recentViewed);
};

$(function () {
    
});

